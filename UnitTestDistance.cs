﻿using Lab_1.ClassLibrary;

namespace Lab_1;

[TestClass]
public class UnitTestDistance
{
    [TestMethod]
    public void TestMethodConstructor()
    {
        Distance obj = new Distance(new Point(10, 20), new Point(20, 40));
    }

    [TestMethod]
    [DataRow(64.28, 100.22, -33.874, 151.213, 11780)]
    [DataRow(64.28, 100.22, 40.71, -74.0, 8335)]

    public void TestMethodGetDistance(double latitude1, double longitude1, double latitude2, double longitude2, double expected)
    {
        // Arrange
        Point p1 = new Point(latitude1, longitude1);
        Point p2 = new Point(latitude2, longitude2);
        Distance obj = new Distance(p1, p2);
        
        // Act
        double result = obj.Result;
        Console.WriteLine($"Расстояние = {result}");
        
        // Assert
        Assert.IsTrue(Math.Abs(expected - Math.Round(obj.Result)) <= 5);
    }
    
    [TestMethod]
    // 9 comment | При расчете расстояния между 2 произвольными точками расстояние должно быть меньше Pi*r, где r - радиус Земли. Здесь хорошо бы проверить точки слева и справа от 180-го меридиана
    [DataRow(0,0, 0, 180, Math.PI * 6371)]
    [DataRow(0,10, 0, -10, Math.PI * 6371)]
    [DataRow(0,-10, 0, 10, Math.PI * 6371)]

    public void TestMethodGetDistanceComment9(double latitude1, double longitude1, double latitude2, double longitude2, double expected)
    {
        // Arrange
        Point p1 = new Point(latitude1, longitude1);
        Point p2 = new Point(latitude2, longitude2);
        Distance obj = new Distance(p1, p2);
        
        // Act
        double result = obj.Result;
        Console.WriteLine($"Расстояние = {result}");
        
        // Assert
        Assert.IsTrue(Math.Round(result) <= expected);
    }
}