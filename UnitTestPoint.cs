using Lab_1.ClassLibrary;

namespace Lab_1;

[TestClass]
public class UnitTestPoint
{
    [TestMethod]
    public void TestMethodConstructor()
    {
        Point obj = new Point(2, 4);
    }

    [TestMethod, ExpectedException(typeof(ArgumentException))]
    public void TestMethodConstructorIncorrect()
    {
        Point obj = new Point(200, 4);
    }
}