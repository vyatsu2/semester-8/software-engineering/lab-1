﻿using Lab_1.ClassLibrary;

namespace Lab_1;

[TestClass]
public class UnitTestAzimuth
{
    [TestMethod]
    public void TestMethodConstructor()
    {
        Azimuth obj = new Azimuth(new Point(10, 20), new Point(20, 40));
    }

    [TestMethod]
    // 2 comment | Для точек, лежащих на прямой, проходящей через центр Земли азимут = any
    [DataRow(-10,-20, 10, 20, AzimuthType.Any)]
    // 3 comment | Для совпадающих точек азимут = none
    [DataRow(10,20, 10, 20, AzimuthType.None)]
    // 5 comment | Для двух точек на одном полюсе азимут = none
    [DataRow(90,20, 90, 20, AzimuthType.None)]
    [DataRow(-90,20, -90, 20, AzimuthType.None)]
    // 6 comment | Для точек на противоположных полюса (см 2) азимут = any
    [DataRow(90,20, -90, 20, AzimuthType.Any)]
    [DataRow(-90,20, 90, 20, AzimuthType.Any)]

    public void TestMethodGetAzimuthByAzimuthType(double latitude1, double longitude1, double latitude2, double longitude2, AzimuthType expected)
    {
        // Arrange
        Point p1 = new Point(latitude1, longitude1);
        Point p2 = new Point(latitude2, longitude2);
        Azimuth obj = new Azimuth(p1, p2);
        
        // Act
        int result = obj.GetAzimuth();
        
        // Assert
        Assert.AreEqual((int) expected, result);
    }

    [TestMethod]
    // Test method
    [DataRow(70,100, 71, 110, 69)]
    [DataRow(10,-60, 50, 121, 359)]
    // 4 comment | Для одной точки на северном полюсе азимут = 180
    [DataRow(90,20, 10, 20, 180)]
    [DataRow(50,20, 90, 20, 180)]
    // 7 comment | Для одной точки на южном полюсе азимут = 180
    [DataRow(50,20, -90, 20, 180)]
    [DataRow(-90,20, -30, 20, 180)]
    // 8 comment | Для точек на экваторе 90 или 270, смотря куда ближе
    [DataRow(0,10, 0, 150, 90)]
    [DataRow(0,0, 0, -160, 270)]
    // 10 comment | Для точек на одном меридиане 0("идем на север") или 180("идем на юг")
    [DataRow(30,100, 10, 100, 180)]
    [DataRow(10,100, 30, 100, 0)]

    public void TestMethodGetAzimuth(double latitude1, double longitude1, double latitude2, double longitude2, double expected)
    {
        // Arrange
        Point p1 = new Point(latitude1, longitude1);
        Point p2 = new Point(latitude2, longitude2);
        Azimuth obj = new Azimuth(p1, p2);
        
        // Act
        double result = obj.GetAzimuth();
        
        // Assert
        Assert.AreEqual(expected, result);
    }
}