﻿namespace Lab_1.ClassLibrary;

public class Point
{
    public double Latitude { get; set; }
    public double Longitude { get; set; }


    public Point(double latitude, double longitude)
    {
        if (longitude is < -180 or > 180 || latitude is < -90 or > 90) throw new ArgumentException();
        Longitude = longitude;
        Latitude = latitude;
    }
}