﻿namespace Lab_1.ClassLibrary;

public class Azimuth
{
    private readonly Point _p1;
    private readonly Point _p2;

    public Azimuth(Point p1, Point p2)
    {
        _p1 = p1;
        _p2 = p2;
    }

    public int GetAzimuth()
    {
        /* Одинаковые точки */
        if (_p1.Latitude == _p2.Latitude && _p1.Longitude == _p2.Longitude)
        {
            return (int)AzimuthType.None;
        }
        
        /* Точки на одной прямой */
        if (_p1.Latitude == -_p2.Latitude && _p1.Longitude == -_p2.Longitude)
        {
            return (int)AzimuthType.Any;
        }

        /* Обе на полюсах, при этом разных */
        if (Math.Abs(_p1.Latitude) == 90 && Math.Abs(_p2.Latitude) == 90 && _p1.Latitude != _p2.Latitude)
        {
            return (int)AzimuthType.Any;
        } 
        
        /* Хотя бы одна на одном из полюсов */
        if (Math.Abs(_p1.Latitude) == 90 || Math.Abs(_p2.Latitude) == 90)
        {
            /* На одном и том же полюсе */
            if (_p1.Latitude == _p2.Latitude)
            {
                return (int)AzimuthType.None;
            }
            return 180;
        }

        double p1Long = DegreesToRadians(_p1.Longitude);
        double p1Lat = DegreesToRadians(_p1.Latitude);
        double p2Long = DegreesToRadians(_p2.Longitude);
        double p2Lat = DegreesToRadians(_p2.Latitude);
        double result = RadiansToDegrees(Math.Atan2(Math.Sin(p2Long - p1Long) * Math.Cos(p2Lat),
            Math.Cos(p1Lat) * Math.Sin(p2Lat) - Math.Sin(p1Lat) *
            Math.Cos(p2Lat) * Math.Cos(p2Long - p1Long)));

        result = Math.Round(result < 0 ? 360 + result : result);
        return (int)result;
    }

    private static double DegreesToRadians(double degrees)
    {
        return degrees * Math.PI / 180;
    }

    private static double RadiansToDegrees(double radians)
    {
        return radians * 180 / Math.PI;
    }
}