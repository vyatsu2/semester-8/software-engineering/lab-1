﻿namespace Lab_1.ClassLibrary;

public class Distance
{
    private Point _start;
    private Point _end;
    public double Result { get; }

    public Distance(Point start, Point end)
    {
        _start = start;
        _end = end;
        Result = GetDistance(start, end);
    }

    private double GetDistance(Point start, Point end)
    {
        double startLat = DegreesToRadians(start.Latitude);
        double startLong = DegreesToRadians(start.Longitude);
        double endLat = DegreesToRadians(end.Latitude);
        double endLong = DegreesToRadians(end.Longitude);
        double cos_d = Math.Sin(startLat) * Math.Sin(endLat) + Math.Cos(startLat) *
            Math.Cos(endLat) *
            Math.Cos(startLong - endLong);
        return Math.Acos(cos_d) * 6371;
    }

    private static double DegreesToRadians(double degrees)
    {
        return degrees * Math.PI / 180;
    }

    private static double RadiansToDegrees(double radians)
    {
        return radians * 180 / Math.PI;
    }
}